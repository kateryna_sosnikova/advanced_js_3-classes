class Employee {

    constructor(person) {
            this.name = person.name,
            this.age = person.age,
            this.salary = person.salary
    }
  
    get newName() {
        return this.name
    }

    set newName(anyName) {
        this.name = anyName
    }

    get newAge() {
        return this.age
    }

    set newAge(anyAge) {
        this.age = anyAge
    }

    get newSalary() {
        return this.salary
    }

    set newSalary(anySalary) {
        this.salary = anySalary
    }

}

class Programmer extends Employee {

    constructor(person) {
        super(person)
        this.lang = person.lang
    }

    get newSalary() {
        return this.salary * 3
    }

    set newSalary(anySalary) {
        this.salary = anySalary 
    }
}

const prog = new Programmer({
    name: 'Petya',
    age: 21,
    salary: 300,
    lang: 'English'
})
console.log(prog);

const prog2 = new Programmer({
    name: 'Sveta',
    age: 50,
    salary: 400,
    lang: 'Ukrainian'
})
console.log(prog2);

const prog3 = new Programmer({
    name: 'Misha',
    age: 38,
    salary: 1000,
    lang: ['Hebrew', 'english', 'russian']
})
console.log(prog3);